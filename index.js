var express = require('express');
var cors = require('cors')
var app = express();
app.use(cors());

app.get('/', function (req, res) {
  res.send({ data1: Math.floor(Math.random() * (5 - 1) + 100), data2: Math.floor(Math.random() * (10 - 1) + 80), code: 200, error: false });
});

app.listen(3000, function () {
  console.log('API inicializada en el puerto 3000');
});